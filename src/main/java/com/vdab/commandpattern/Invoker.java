package com.vdab.commandpattern;

import lombok.Getter;

@Getter

public enum Invoker {
    OPTION1(1,"1. show option 1",new Command1())
    ,OPTION2(2,"2. show option 2",new Command2())
    ,OPTION3(3,"3. show option 3",new Command3())
    ,OPTION4(4,"4. show option 4",new Command4())
    ,OPTION5(5,"4. show option 5",new Command5());

    private final int id;
    private final String displayOption;
    private final Command myCommand;

    Invoker(int id , String displayOption, Command myCommand ){

        this.id = id;
        this.displayOption = displayOption;
        this.myCommand = myCommand;

    }


}
