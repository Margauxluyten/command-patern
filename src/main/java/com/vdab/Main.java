package com.vdab;

import com.vdab.commandpattern.Command;
import com.vdab.commandpattern.Invoker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    private static List<Invoker> commandList = Arrays.stream(Invoker.values()).collect(Collectors.toList());
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        //Invoker.values()[scanner.nextInt() -1 ].getMyCommand().execute();
        while (true) {
            try {
                commandList.forEach(invoker -> System.out.println(invoker.getDisplayOption()));
                System.out.println();
                System.out.println("Choose an option : ");
                chooseAnOption();
            } catch (Exception e) {
                System.out.println("Error" + e.getMessage());
                scanner.nextLine();
            } finally {
                System.out.println();
                System.out.println("Do you want to continue ? (yes/no) ");
                if(scanner.next().equals("no")){
                    break;
                }
            }

        }
    }

    private static void chooseAnOption() throws Exception {

        int optionId = scanner.nextInt();
        commandList.stream().filter(invoker -> invoker.getId() == optionId)
                .findFirst()
                .orElseThrow(() -> new Exception("option not found"))
                .getMyCommand()
                .execute();

    }

}


